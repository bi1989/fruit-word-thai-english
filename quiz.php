<?php 
//header('Content-Type: text/html; charset=utf-8');


$Questions = array(
    1 => array(
        'Question' => 'Banana',
        'Answers' => array(
            'A' => 'มะขาม',
            'B' => 'กล้วย',
            'C' => 'ทุเรียนs'
        ),
        'CorrectAnswer' => 'B'
    ),
    2 => array(
    'Question' => 'Pineapple',
    'Answers' => array(
        'A' => 'สัปปะรด',
        'B' => 'ส้มเขียวหวาน',
        'C' => 'มะไฟ'
    ),
    'CorrectAnswer' => 'A'
    ),
    3 => array(
    'Question' => 'มังคุด',
    'Answers' => array(
        'A' => 'Mango',
        'B' => 'Mangosteen',
        'C' => 'Papaya'
    ),
    'CorrectAnswer' => 'B'
    ),
    4 => array(
    'Question' => 'Cantaloupe',
    'Answers' => array(
        'A' => 'ฝรั่ง',
        'B' => 'แตงโม',
        'C' => 'แคนตาลูป'
    ),
    'CorrectAnswer' => 'C'
    ),
    5 => array(
    'Question' => 'Star fruit',
    'Answers' => array(
        'A' => 'มะเฟือง',
        'B' => 'มะม่วง',
        'C' => 'มะขาม'
    ),
    'CorrectAnswer' => 'A'
    ),
    6 => array(
    'Question' => 'ชมพู่',
    'Answers' => array(
        'A' => 'Java apple',
        'B' => 'Dragon fruit',
        'C' => 'Coconut'
    ),
    'CorrectAnswer' => 'A'
    ),
    7 => array(
    'Question' => 'Sugar apple',
    'Answers' => array(
        'A' => 'น้อยหน่า',
        'B' => 'กระท้อน',
        'C' => 'เสาวรส'
    ),
    'CorrectAnswer' => 'A'
    ),
    8 => array(
    'Question' => 'มะเดื่อฝรั่ง',
    'Answers' => array(
        'A' => 'Watermelon',
        'B' => 'Cantaloupe',
        'C' => 'Fig'
    ),
    'CorrectAnswer' => 'C'
    ),
    9 => array(
    'Question' => 'ลิ้นจี่',
    'Answers' => array(
        'A' => 'Sapodilla',
        'B' => 'Lychee',
        'C' => 'Longkong'
    ),
    'CorrectAnswer' => 'B'
    ),
    10 => array(
    'Question' => 'Tangerine',
    'Answers' => array(
        'A' => 'ส้ม',
        'B' => 'ส้มเขียวหวาน',
        'C' => 'ส้มโอ'
    ),
    'CorrectAnswer' => 'B'
    ),
    11 => array(
    'Question' => 'Sapodilla',
    'Answers' => array(
        'A' => 'ละมุด',
        'B' => 'ลำไย',
        'C' => 'ส้มเขียวหวาน'
    ),
    'CorrectAnswer' => 'A'
    ),
    12 => array(
    'Question' => 'เสาวรส',
    'Answers' => array(
        'A' => 'Sala',
        'B' => 'Pineapple',
        'C' => 'Passion fruit'
    ),
    'CorrectAnswer' => 'C'
    ),
    13 => array(
    'Question' => 'แก้วมังกร',
    'Answers' => array(
        'A' => 'Dragon fruit',
        'B' => 'Pomegranate',
        'C' => 'Watermelon'
    ),
    'CorrectAnswer' => 'A'
    ),
    14 => array(
    'Question' => 'แตงโม',
    'Answers' => array(
        'A' => 'Pineapple',
        'B' => 'Watermelon',
        'C' => 'Santol'
    ),
    'CorrectAnswer' => 'B'
    ),
    15 => array(
    'Question' => 'Durian',
    'Answers' => array(
        'A' => 'ขนุน',
        'B' => 'ทุเรียน',
        'C' => 'เงาะ'
    ),
    'CorrectAnswer' => 'B'
    ),
    16 => array(
    'Question' => 'Persimmon',
    'Answers' => array(
        'A' => 'ลูกพลับ',
        'B' => 'ลูกตาล',
        'C' => 'มะเดื่อฝรั่ง'
    ),
    'CorrectAnswer' => 'A'
    ),
    17 => array(
    'Question' => 'Dragon fruit',
    'Answers' => array(
        'A' => 'พุทรา',
        'B' => 'แก้วมังกร',
        'C' => 'สัปปะรด'
    ),
    'CorrectAnswer' => 'B'
    ),
    18 => array(
    'Question' => 'Longkong',
    'Answers' => array(
        'A' => 'ลองกอง',
        'B' => 'มะเหมี่ยว',
        'C' => 'กระท้อน'
    ),
    'CorrectAnswer' => 'A'
    ),
    19 => array(
    'Question' => 'ส้มโอ',
    'Answers' => array(
        'A' => 'Pummelo',
        'B' => 'Pomegranate',
        'C' => 'Musk melon'
    ),
    'CorrectAnswer' => 'A'
    ),
    20 => array(
        'Question' => 'Guava',
        'Answers' => array(
            'A' => 'ทุเรียน',
            'B' => 'ลิ้นจี่',
            'C' => 'ฝรั่ง'
        ),
        'CorrectAnswer' => 'C'
    )
);

if (isset($_POST['answers'])){
    $Answers = $_POST['answers']; // Get submitted answers.

    // Now this is fun, automated question checking! ;)
    echo '<head>
            <title>คำศัพท์ผลไม้</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        </head>
        <body><h3>สีเขียวหมายถึงตอบถูก สีแดงหมายถึงตอบผิด</h3></body>';
    foreach ($Questions as $QuestionNo => $Value){
        // Echo the question

        echo $Value['Question'].'<br />';

        if ($Answers[$QuestionNo] != $Value['CorrectAnswer']){
            echo '<span style="color: red;">'.$Value['Answers'][$Answers[$QuestionNo]].'</span>'; // Replace style with a class
        } else {
            echo '<span style="color: green;">'.$Value['Answers'][$Answers[$QuestionNo]].'</span>'; // Replace style with a class
        }
        echo '<br /><hr>';
    }   
    echo '<center><h2><a href="index.html">กลับหน้าหลัก</a> || <a href="javascript:history.back()">กลับไปทำใหม่</a></h2></center>';
} else {
?>
    <html>
        <head>
            <title>คำศัพท์ผลไม้</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        </head>
        <body>
            <h2>จงเลือกคำตอบที่ถูกต้อง</h2>
            <form action="quiz.php" method="post" id="quiz">
            <?php foreach ($Questions as $QuestionNo => $Value){ ?>
            <li>
                <b><?php echo $Value['Question']; ?></b>
                <?php 
                    foreach ($Value['Answers'] as $Letter => $Answer){ 
                    $Label = 'question-'.$QuestionNo.'-answers-'.$Letter;
                ?>
                <div>
                    <input type="radio" name="answers[<?php echo $QuestionNo; ?>]" id="<?php echo $Label; ?>" value="<?php echo $Letter; ?>" />
                    <label for="<?php echo $Label; ?>"><?php echo $Letter; ?>) <?php echo $Answer; ?> </label>
                </div>
                <?php } ?>
            </li>
            <br>
            <?php } ?>
            <input type="submit" value="ส่งคำตอบ" />
            </form>
            <center><h2><a href="index.html">กลับหน้าหลัก</a></h2></center>
        </body>
    </html>
<?php 
}
?>